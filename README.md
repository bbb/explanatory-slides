# BBB Default Slides

This is a fork of Senfcall's default slides for BigBlueButton.
The icons are from the font included in the BigBlueButton repository itself (https://github.com/bigbluebutton/bigbluebutton-font) and therefore are published under the GNU LESSER GENERAL PUBLIC LICENSE Version 3 which is also included in this repo.
The Stratum0-Logo is provided by blinry, see https://stratum0.org/wiki/Logo#Lizenz
The font used is Droid Sans.
